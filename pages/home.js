/* global axios, md5 */
import html from './home.html'
import 'ui@body/body.js'
import 'ui@appbar/appbar.js'
import 'ui@button/button.js'
import 'ui@input/input.js'
import 'ui@sidebar/sidebar.js'
import 'ui@dialog/dialog.js'
import api from '../api/api.js'
import 'node@blueimp-md5/js/md5.min.js'

export default {
  template: html,
  data: function () {
    return {
      items: [],
      show: false,
      showLogin: true,
      name: '',
      username: '',
      password: '',
      showFixed: false,
      showDialog: false,
      buttons: ['确定'],
      hasVoted: false,
      article: {}
    }
  },
  onMessage: function (event) {
    if (event.message === 'reload') {
      this.reload()
    }
  },
  created: function () {
    this.name = localStorage.getItem('username')
    if (this.name) {
      this.showLogin = false
    }
  },
  mounted: function () {
    var me = this
    axios.get(api.list + '?t=' + Date.now()).then(function (data) {
      if (data.data[0].votedid.indexOf(me.name) !== -1) {
        me.hasVoted = true
      } else {
        me.hasVoted = false
      }
      me.items = data.data.sort(function (a, b) {
        return b.sort - a.sort
      })
      for (var i = 0, l = data.data.length; i < l; i++) {
        if (data.data[i].username === me.name) {
          me.article = data.data[i]
          break
        }
      }
    }).catch(function (e) {
      alert(e)
    })

    me.$nextTick(function () {
      me.$refs.jdiv.jroll.on('scroll', function () {
        if (this.y < -10) {
          me.showFixed = true
        } else {
          me.showFixed = false
        }
      })
      me.$refs.jdiv.jroll.on('scrollEnd', function () {
        if (this.y < -10) {
          me.showFixed = true
        } else {
          me.showFixed = false
        }
      })
    })
  },
  methods: {
    goAdd: function () {
      var me = this
      if (me.hasVoted) {
        me.go('pages/add', {
          name: me.name,
          article: me.article
        })
      } else {
        me.showDialog = true
      }
    },
    showRute: function () {
      this.show = true
    },
    colseSideBar: function () {
      this.show = false
    },
    login: function () {
      var me = this
      if (this.username === '') {
        alert('请输入用户名')
        return
      }
      if (this.password === '') {
        alert('请输入密码')
        return
      }
      axios.get(api.login + '?username=' + me.username + '&password=' + md5(me.password) + '&t=' + Date.now()).then(function (result) {
        if (result.data === 'success') {
          localStorage.setItem('username', me.username)
          me.name = me.username
          me.reload()
        } else if (result.data === 'fail') {
          alert('密码不正确')
        }
      })
    },
    logout: function () {
      localStorage.removeItem('username')
      this.reload()
    },
    clickDialog: function () {
      this.showDialog = false
    },
    findVotedId: function (ids) {
      if (ids.indexOf(this.name) !== -1) {
        return true
      } else {
        return false
      }
    },
    open: function (id, url, name) {
      var me = this
      axios.get(api.vote + '?id=' + id + '&name=' + name + '&t=' + Date.now()).then(function () {
        me.hasVoted = true
      })
      location.href = url
    },
    reload: function () {
      location.href = location.href.replace(/\?t=\d+/, '').replace('#pages/home', '') + '?t=' + Math.random().toString().substr(2)
    }
  }
}