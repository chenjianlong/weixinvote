import html from './add.html'
import 'ui@input/input.js'
import api from '../api/api.js'

export default {
  template: html,
  data: function () {
    return {
      title: '',
      link: '',
      vote: '',
      name: ''
    }
  },
  afterEnter: function (params) {
    if (params) {
      this.name = params.name
      this.title = params.article.title
      this.link = params.article.link
      this.vote = params.article.vote
    }
  },
  methods: {
    goBack: function () {
      this.go(-1)
    },
    add: function () {
      var me = this
      if (me.title === '') {
        alert('请输入标题')
        return
      }
      if (me.link === '') {
        alert('请输入链接')
        return
      }
      if (!/^\d+$/.test(me.vote)) {
        alert('码号只允许为数字')
        return
      }
      axios.get(api.add + '?title=' + me.title + '&link=' + me.link + '&name=' + me.name + '&vote=' + me.vote).then(function (result) {
        me.go(-1)
        setTimeout(function () {
          me.postMessage('reload', 'pages/home')
        }, 300)
      })
    }
  }
}