// var host = 'http://192.168.1.105:8888'
var host = 'http://localhost:8888'

export default {
  'list': host + '/list',
  'login': host + '/login',
  'vote': host + '/vote',
  'add': host + '/add'
}
