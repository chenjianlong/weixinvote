var jtaroBundle = require('jtaro-bundle')
// var uglify = require('rollup-plugin-uglify')
var alias = require('rollup-plugin-paths')

jtaroBundle.bundle({
  origin: 'index.html', // 开发目录的index.html
  target: 'www/index_template.html', // 生产目录的index.html模板
  copies: [], // 直接拷贝的文件（夹）
  // 自定义使用rollup打包时使用的插件
  rollupPlugins: [alias({
    'node@': './node_modules/', // 以入口文件所在路径为基准
    'ui@': './node_modules/jtaro-ui/components/'
  })],
  callback: function () {
    console.log('打包完成')
  }
})
