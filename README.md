# weixinvote

微信互投项目，为了投票而做的项目，也是为了挑战自我做的项目

## 初始化项目

```
npm install
```

## 运行后台服务

```
npm run server
```

## 运行开发

在新的命令行
```
npm run dev
```

访问`localhost:8099/`，这样只能访问登录界面，有两个方案可运行全部

- 方案一：注释`server.js`里`判断是否在微信`的代码，并重新执行`npm run server`
- 方案二：将`api/api.dev.js`里的`localhost`换成你的局域网地址，手机连局域网，用微信访问

## 打包

```
npm run build
```

然后运行`localhost:8888/www/`
