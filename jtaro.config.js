// jtaro.module.config.js
var alias = require('rollup-plugin-paths')

module.exports = {
  website: '../../', // 站点目录，以server.js所在路径为基准
  entry: '../../pages.js', // 入口文件，以server.js所在路径为基准
  plugins: [alias({
    'node@': './node_modules/', // 以入口文件所在路径为基准
    'ui@': './node_modules/jtaro-ui/components/'
  })]
}
